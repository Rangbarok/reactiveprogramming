const opencage = require('opencage-api-client');
const openGeocoder = require('node-open-geocoder');
var robot = require("robotjs");
var Rxjs = require('rxjs');
var operators = require('rxjs/operators');

const numberOfCursorRequest = 100;
var auxLoop = 0;

const screenSize = robot.getScreenSize();
const heightCentral = screenSize.height / 2;
const widthCentral = screenSize.width / 2;

var oldPos = {
    x: 0,
    y: 0
};
const startDate = Date.now();
console.log("Initial time :: ", startDate);
const obs = Rxjs.Observable.create(function (observer) {
    setInterval(() => {
        observer.next(robot.getMousePos());

        // if(auxLoop < numberOfCursorRequest) {
        //     observer.next(robot.getMousePos());
        //     // auxLoop++;
        // }
    }, 100);
    // Es necesario completas el observer para que entre en el evento final de cada subscribe??
});

// PARALLEL mode
const obsMergeMap = obs.pipe(
    operators.filter(val => (val.x !== oldPos.x && Math.abs(val.x - oldPos.x) > 10) || (val.y !== oldPos.y && Math.abs(val.y - oldPos.y) > 10)),
    operators.map(val => converseMouseToCoordinates(val)),
    operators.mergeMap(val => getCoordinatesInfoAsync(val)),
    operators.filter(val => val.ccInfoValue !== -1 && val.ccInfoValue !== 0)
)
.subscribe(
    val => {
        console.log("MergeMap data:: ", val.ccInfoString);
    },
    err => {
        console.log(err);
    },
    () => {
        const endDate = Date.now();
        console.log("Final time MergeMap :: ", endDate);
        console.log("Total time in ms :: ", endDate - startDate);
    }
);

// SERIAL mode
const obsConcatMap = obs.pipe(
    operators.filter(val => (val.x !== oldPos.x && Math.abs(val.x - oldPos.x) > 10) || (val.y !== oldPos.y && Math.abs(val.y - oldPos.y) > 10)),
    operators.map(val => converseMouseToCoordinates(val)),
    operators.concatMap(val => getCoordinatesInfoAsync(val)),
    operators.filter(val => val.ccInfoValue !== -1 && val.ccInfoValue !== 0)
)
.subscribe(
    val => {
        console.log("ConcatMap data:: ", val.ccInfoString);
    },
    err => {
        console.log(err);
    },
    () => {
        const endDate = Date.now();
        console.log("Final time ConcatMap :: ", endDate);
        console.log("Total time in ms :: ", endDate - startDate);
    }
);

// MUTE mode
const obsExhaustMap = obs.pipe(
    operators.filter(val => (val.x !== oldPos.x && Math.abs(val.x - oldPos.x) > 10) || (val.y !== oldPos.y && Math.abs(val.y - oldPos.y) > 10)),
    operators.map(val => converseMouseToCoordinates(val)),
    operators.exhaustMap(val => getCoordinatesInfoAsync(val)),
    operators.filter(val => val.ccInfoValue !== -1 && val.ccInfoValue !== 0)
)
.subscribe(
    val => {
        console.log("Exhaust data:: ", val.ccInfoString);
    },
    err => {
        console.log(err);
    },
    () => {
        const endDate = Date.now();
        console.log("Final time ExhaustMap :: ", endDate);
        console.log("Total time in ms :: ", endDate - startDate);
    }
);

// CUTOFF mode
const obsSwitchMap = obs.pipe(
    operators.filter(val => (val.x !== oldPos.x && Math.abs(val.x - oldPos.x) > 10) || (val.y !== oldPos.y && Math.abs(val.y - oldPos.y) > 10)),
    operators.map(val => converseMouseToCoordinates(val)),
    operators.switchMap(val => getCoordinatesInfoAsync(val)),
    operators.filter(val => val.ccInfoValue !== -1 && val.ccInfoValue !== 0)
)
.subscribe(
    val => {
        console.log("SwitchMap data:: ", val.ccInfoString);
    },
    err => {
        console.log(err);
    },
    () => {
        const endDate = Date.now();
        console.log("Final time SwitchMap :: ", endDate);
        console.log("Total time in ms :: ", endDate - startDate);
    }
);

function converseMouseToCoordinates(mousePosition) {
    oldPos.x = mousePosition.x;
    oldPos.y = mousePosition.y;
    var latitude;

    if (mousePosition.y > heightCentral) {
        // South
        latitude = (-1) * ((mousePosition.y - heightCentral) / heightCentral) * 90;
    } else {
        // North
        latitude = (1 - ((mousePosition.y) / heightCentral)) * 90;
    }

    var longitude;
    if (mousePosition.x > heightCentral) {
        // East
        longitude = ((mousePosition.x - widthCentral) / widthCentral) * 180;
    } else {
        // West
        longitude = (-1) * (1 - ((mousePosition.x) / widthCentral)) * 180;
    }

    // auxLoop++;

    return {
        longitude: longitude,
        latitude: latitude
    };
};

function getCoordinatesInfoAsync(coordinates) {
    return new Promise((resolve, reject) => {

        var ccInfoValue = -1;
        var ccInfoString = '';
        var returnObj = {};

        var geocoderObj = {
            q: '' + coordinates.latitude + ',' + coordinates.longitude,
            language: 'en'
        };

        opencage.geocode(geocoderObj).then(data => {
            if (data.status.code == 200) {
                if (data.results.length > 0) {
                    var place = data.results[0];
                    if (place.components._type === 'body_of_water') {
                        // console.log("Water ::" ,place.components.body_of_water);
                        ccInfoString = "Water ::" + place.components.body_of_water;
                        ccInfoValue = 0;
                        returnObj = {
                            ccInfoValue: ccInfoValue,
                            ccInfoString: ccInfoString
                        }
                        resolve(returnObj);
                    } else if (place.components.country && place.components.state) {
                        // console.log("Country :: ", place.components.country);
                        // console.log("State :: ", place.components.state);
                        ccInfoString = "Country :: " + place.components.country + '\n' + "State :: " + place.components.state;
                        ccInfoValue = 1;
                        returnObj = {
                            ccInfoValue: ccInfoValue,
                            ccInfoString: ccInfoString
                        }
                        resolve(returnObj);
                    } else if (place.components.country) {
                        // console.log("Country :: ", place.components.country);
                        ccInfoString = "Country :: " + place.components.country;
                        ccInfoValue = 2;
                        returnObj = {
                            ccInfoValue: ccInfoValue,
                            ccInfoString: ccInfoString
                        }
                        resolve(returnObj);
                    } else {
                        ccInfoString = "Error:: unknown place";
                        ccInfoValue = -1;
                        returnObj = {
                            ccInfoValue: ccInfoValue,
                            ccInfoString: ccInfoString
                        }
                        reject(returnObj);
                    }
                }
            }
        }).catch(error => {
            // console.log('error', error.message);
            ccInfoString = "Error:: " + error.message;
            ccInfoValue = -1;
            returnObj = {
                ccInfoValue: ccInfoValue,
                ccInfoString: ccInfoString
            }
            reject(returnObj);
        });
    });

};

