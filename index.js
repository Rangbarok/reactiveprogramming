const opencage = require('opencage-api-client');
const openGeocoder = require('node-open-geocoder');
var robot = require("robotjs");
import {of, from} from 'rxjs';

const numberOfCursorRequest = 10;

const screenSize = robot.getScreenSize();
const heightCentral = screenSize.height / 2;
const widthCentral = screenSize.width / 2;

var oldPos = {
    x: 0,
    y: 0
};

var auxLoop = 0;
while (auxLoop < numberOfCursorRequest) {
    var currentPos = robot.getMousePos();
    var coordinates = converseMouseToCoordinates(currentPos);
    var countryString = getCountryReverse(coordinates);
}

function converseMouseToCoordinates(mousePosition) {
    if (oldPos.x !== currentPos.x || oldPos.y !== currentPos.y) {
        oldPos.x = currentPos.x;
        oldPos.y = currentPos.y;
        var latitude;

        if(currentPos.y > heightCentral) {
            // South
            latitude = (-1)*((currentPos.y - heightCentral) / heightCentral) * 90;
        } else {
            // North
            latitude = (1-((currentPos.y) / heightCentral)) * 90;
        }

        var longitude;
        if(currentPos.x > heightCentral) {
            // East
            longitude = ((currentPos.x - widthCentral) / widthCentral) * 180;
        } else {
            // West
            longitude = (-1)*(1-((currentPos.x) / widthCentral)) * 180;
        }

        auxLoop++;
    }
};

function getCountryReverse(coordinates) {

    var geocoderObj = {
        q : '' + latitude +',' + longitude,
        language: 'en'
    };

    opencage.geocode(geocoderObj).then(data => {
        if (data.status.code == 200) {
          if (data.results.length > 0) {
            var place = data.results[0];
            if(place.components._type === 'body_of_water') {
                console.log("WATER ::" ,place.components.body_of_water);
                return -1;
            } else if(place.components.country && place.components.state){
                console.log("COUNTRY :: ", place.components.country);
                console.log("STATE :: ", place.components.state);
                return 1;
            } else if(place.components.country) {
                console.log("COUNTRY :: ", place.components.country);
                return 2;
            }
          }
        }
      }).catch(error => {
        console.log('error', error.message);
      });
};